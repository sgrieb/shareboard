var gulp = require('gulp');
var clean = require('gulp-clean');
var typescript = require('gulp-tsc');
const shell = require('gulp-shell')
 
 
gulp.task('copy', function() {
  return gulp.src('./src/**/*')
        .pipe(gulp.dest('./build/'));
});
 
gulp.task('clean', function () {
    return gulp.src('./build', {read: false, force: true})
        .pipe(clean());
});

gulp.task('build-typescript', shell.task([
  'tsc -p build/'
]));

gulp.task('build', gulp.series('clean', 'copy', 'build-typescript'));

gulp.task('watcher', function(){
  return gulp.watch('src/**/*', gulp.series('build'));
});
