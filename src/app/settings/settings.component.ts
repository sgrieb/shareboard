import { Component } from '@angular/core';
import { StateService } from '../services/state.service';
import { User } from '../models/user';

@Component({
  styleUrls: ['./settings.css'],
  selector: 'sb-settings',
  templateUrl: './settings.html'
})

export class SettingsComponent {
  user:User;
  
  constructor(private stateService:StateService) {
    this.user = stateService.getUser();
    stateService.onUser.subscribe(this.onUser.bind(this));
  }

  onUser(user:User) {
    this.user = user;
  }
}
