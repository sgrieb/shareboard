export class Settings {
    // max time in seconds
   public static MAX_TIME=30;
   // default time in seconds
   public static DEFAULT_TIME=10;
   // sound location
   public static SOUND_LOCATION='http://localhost/~steve/sounds/';
}