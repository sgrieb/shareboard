import { Component } from '@angular/core';
import { User } from '../models/user';
import { ApiService } from '../services/api.service';
import { StateService } from '../services/state.service';
import { Router } from '@angular/router';

@Component({
  styleUrls: ['./login.css'],
  selector: 'login',
  templateUrl: './login.html'
})

export class LoginComponent {
  user:User;
  
  constructor(
    private apiService:ApiService, 
    private router:Router, 
    private stateService:StateService) 
    {
      this.user = new User();
  }

  submit() {
    this.apiService.addUser(this.user)
      .then(this.onLogin.bind(this), this.onError.bind(this));
  }

  onLogin(user:User) {
    this.stateService.setUser(user);
    this.router.navigateByUrl('/');
  }

  onError(error:any) {
  }
}
