import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import {Ng2Webstorage} from 'ngx-webstorage';
import { FormsModule }    from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule }    from '@angular/http';
import { CollapseModule } from 'ngx-bootstrap';

import { AppRoutingModule }        from './app-routing.module';
import { AppComponent }        from './app.component';
import { MediaVideo }        from './video/video';
import { NavComponent }        from './nav/nav.component';

import { SoundsComponent } from './sounds/sounds.component';
import { SoundComponent } from './sound/sound.component';
import { BoardsComponent } from './boards/boards.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SettingsComponent } from './settings/settings.component';

import { ApiService } from './services/api.service';
import { AddSoundService } from './services/addSound.service';
import { StorageService } from './services/storage.service';
import { StateService } from './services/state.service';
import { Settings }        from './util/settings';
import {Angular2PromiseButtonModule} from 'angular2-promise-buttons';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    Ng2Webstorage,
    Angular2PromiseButtonModule.forRoot({
      // your custom config goes here
      spinnerTpl: '<span class="spinner-container"><i class="button-spinner fa fa-spinner fa-spin" style="font-size:20px"></i></span>',
      // disable buttons when promise is pending
      disableBtn: true,
      // the class used to indicate a pending promise
      btnLoadingClass: 'is-loading',
      // only disable and show is-loading class for clicked button, 
      // even when they share the same promise
      handleCurrentBtnOnly: false,
    }),
    CollapseModule.forRoot()
  ],
  declarations: [
    AppComponent,
    MediaVideo,
    NavComponent,
    SoundsComponent,
    BoardsComponent,
    HomeComponent,
    LoginComponent,
    SoundComponent,
    RegisterComponent,
    SettingsComponent
  ],
  providers: [
    Settings,
    AddSoundService,
    ApiService,
    StorageService,
    StateService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  
}
