import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SoundComponent } from './sound/sound.component';
import { SoundsComponent } from './sounds/sounds.component';
import { HomeComponent } from './home/home.component';
import { BoardsComponent } from './boards/boards.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  { 
    path: '', 
    redirectTo: '/', 
    pathMatch: 'full'
  },
  { 
    path: '', 
    component: HomeComponent
  },
  {
    path: 'sound',
    component: SoundComponent
  },
  {
    path: 'sound/:id',
    component: SoundComponent
  },
  {
    path: 'sounds',
    component: SoundsComponent
  },
  {
    path: 'boards',
    component: BoardsComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
