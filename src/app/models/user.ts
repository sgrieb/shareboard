import {Registry} from './registry';

export class User {
    realm: string
    username: "string"
    email: "string"
    emailVerified: true
    id: -1
}

Registry.register('User', User);