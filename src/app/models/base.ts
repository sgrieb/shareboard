import {Registry} from './registry';

export class Base {
    
    static create(item:any) {
        Object.assign(new Base(), item)
    }

    static createArray(items:any) {
        let klass = Registry.models[this.name];
        let newItems = new Array<any>();
        items.forEach((item:any) => {
            newItems.push(Object.assign(new klass(), item));
        });
        return newItems;
    }
}