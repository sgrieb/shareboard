import {Sound} from './sound'
import {Registry} from './registry';

export class Board {
    name:string
    sounds:Sound[]
    imgPath:string
}

Registry.register('Board', Board);
