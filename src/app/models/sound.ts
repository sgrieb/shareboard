import {Settings} from '../util/settings';
import {Base} from './base';
import {Registry} from './registry';

export class Sound extends Base {
    id:string
    path: string
    imgPath:string
    url:string = 'https://www.youtube.com/watch?v=ucoTDhIcmGY'
    start:number = 0;
    name:string
    end:number = Settings.DEFAULT_TIME;

    get videoId () {
        return this.url.split('?v=')[1];
    }

    get duration () {
        return this.end - this.start;
    }

    get src () {
        return Settings.SOUND_LOCATION + this.id + '.mp3';
    }
}

Registry.register('Sound', Sound);