
export class Registry {
    static models: any = {};

    static register (name:string, model:any) {
        Registry.models[name] = model;
    }
}