import { Component, AfterViewInit } from '@angular/core';
import { AddSoundService } from '../services/addSound.service';

declare const window:any;

@Component({
  styleUrls: ['./video.css'],
  selector: 'media-video',
  templateUrl: './video.html'
})

export class MediaVideo implements AfterViewInit {
  addSoundService:AddSoundService;

  constructor(addSoundService: AddSoundService) {
    this.addSoundService = addSoundService;
  }

  ngAfterViewInit() {
    let tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    
    let firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady = this.addSoundService.onYouTubeIframeAPIReady.bind(this.addSoundService);
  }
}
