import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Settings } from '../util/settings';
import { Sound } from '../models/sound';

declare const document:any;

@Component({
  styleUrls: ['./sounds.css'],
  selector: 'sounds',
  templateUrl: './sounds.html'
})

export class SoundsComponent implements OnInit {
  sounds:any[];

  constructor(private apiService:ApiService, private settings:Settings) {

  }

  ngOnInit() {
    this.apiService.getSounds().then(this.onSounds.bind(this));
  }

  onSounds(sounds:Array<any>) {
      this.sounds = Sound.createArray(sounds);
  }

  play(event:any, sound:any) {
    let soundElem = document.getElementById(event.target.id.replace('-play', '') + '-sound');
    soundElem.play();

    sound.isPlaying = true;

    soundElem.onended = function() {
        sound.isPlaying = false;
        soundElem.load();
    };

    event.stopPropagation();
  }

  stop(event:any, sound:any) {
    let soundElem = document.getElementById(event.target.id.replace('-stop', '') + '-sound');
    soundElem.pause();
    soundElem.load();

    sound.isPlaying = false;
    delete soundElem.onended;

    event.stopPropagation();
  }
}
