import { Component } from '@angular/core';
import { MediaVideo } from './video/video';
import { StateService } from './services/state.service';

@Component({
  styleUrls: ['./app.css'],
  selector: 'app',
  templateUrl: './app.html'
})

export class AppComponent {

  constructor(private stateService:StateService) {
    stateService.init();
  }


}