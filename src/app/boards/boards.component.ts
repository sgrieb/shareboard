import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {Board} from '../models/board';
import { ApiService } from '../services/api.service';

@Component({
  styleUrls: ['./boards.css'],
  selector: 'boards',
  templateUrl: './boards.html'
})

export class BoardsComponent {
  boards: Board[];
  apiService:ApiService;

  constructor(apiService:ApiService) {
    this.apiService = apiService;
  }

  ngOnInit() {
    this.apiService.getBoards().then(this.onBoards.bind(this));
  }

  onBoards(boards:Board[]) {
    this.boards = boards;
  }
}
