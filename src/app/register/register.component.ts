import { Component } from '@angular/core';
import {Router} from '@angular/router';

import { User } from '../models/user';
import { ApiService } from '../services/api.service';
import { StateService } from '../services/state.service';

@Component({
  styleUrls: ['./register.css'],
  selector: 'register',
  templateUrl: './register.html'
})

export class RegisterComponent {
  user:User;
  
  constructor (
    private apiService:ApiService, 
    private router:Router, 
    private stateService:StateService) 
    {
      this.user = new User();
  }

  submit() {
    this.apiService.addUser(this.user)
      .then(this.onRegister.bind(this), this.onError.bind(this));
  }

  onRegister(user:User) {
    this.stateService.setUser(user);
    this.router.navigateByUrl('/');
  }

  onError(error:any) {
  }
}
