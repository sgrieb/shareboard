import { Component } from '@angular/core';
import { StateService } from '../services/state.service';
import { User } from '../models/user';

@Component({
  styleUrls: ['./nav.css'],
  selector: 'sb-nav',
  templateUrl: './nav.html'
})

export class NavComponent {
  public isCollapsed: boolean = true;
  private user:User = null;

  constructor(private stateService:StateService) {
    this.user = stateService.getUser();
    stateService.onUser.subscribe(this.onUser.bind(this));
  }

  onUser(user:User) {
    this.user = user;
  }

  collapse() {
    this.isCollapsed = true;
  }
}
