import { Injectable } from '@angular/core';
import { User } from '../models/user';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';

const USER_KEY = 'User';

@Injectable()
export class StorageService {

  constructor(private localStorageService:LocalStorageService) {}

  setUser(user:User) {
    this.storeLocal(USER_KEY, user);
  }

  getUser() {
    return this.getLocal(USER_KEY);
  }

  private storeLocal(key:string, value:any) {
    return this.localStorageService.store(key, JSON.stringify(value));
  }

  private getLocal(key:string) {
    return JSON.parse(this.localStorageService.retrieve(key));
  }
}