import { Injectable, Output, EventEmitter } from '@angular/core';
import { User } from '../models/user';
import { StorageService } from './storage.service';

@Injectable()
export class StateService {
    @Output() onUser = new EventEmitter();
    user:User = null;

    constructor(private storageService:StorageService) {
    }

    init() {
        this.user = this.storageService.getUser();
        this.onUser.emit(this.user);
    }

    setUser(user:User) {
        this.user = user;
        this.storageService.setUser(user);
        this.onUser.emit(this.user);
    }

    getUser() {
        return this.user;
    }

}