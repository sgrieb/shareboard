import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Sound } from '../models/sound';
import { User } from '../models/user';

const BASE_URL = 'http://localhost:4000/api';
const HEADERS = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class ApiService {
  
  constructor(private http: Http) {}

  getSounds() {
    return this.http.get(BASE_URL + '/sounds')
      .map(res => res.json()).toPromise();
  }

  getSound(id:string) {
    return this.http.get(BASE_URL + '/sounds/' + id)
      .map(res => res.json()).toPromise();
  }

  addSound(sound:Sound) {
    delete sound.id;
    return this.http.post(BASE_URL + '/sounds/addSound', {sound:sound}, HEADERS)
      .map(res => res.json()).toPromise();
  }

  getBoards() {
    return this.http.get(BASE_URL + '/boards')
      .map(res => res.json()).toPromise();
  }

  addUser(user:User) {
    delete user.id;
    return this.http.post(BASE_URL + '/Users', user, HEADERS)
      .map(res => res.json()).toPromise();
  }

  loginUser(user:User) {
    delete user.id;
    return this.http.post(BASE_URL + '/Users/login', user, HEADERS)
      .map(res => res.json()).toPromise();
  }
}