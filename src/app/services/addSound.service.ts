import {Injectable} from '@angular/core';
import {Sound} from '../models/sound'

declare const YT:any;

@Injectable()
export class AddSoundService {
    player:any;
    sound:Sound;

    constructor() {
    }

    playVideo(sound:Sound) {
        this.sound = sound;

        this.player.loadVideoById(sound.videoId, sound.start);
    }

    stopVideo() {
        this.player.stopVideo();
    }

    onYouTubeIframeAPIReady() {
        this.player = new YT.Player('player', {
            height: '300',
            width: '300',
            events: {
            'onReady': this.onPlayerReady.bind(this),
            'onStateChange': this.onPlayerStateChange.bind(this)
            }
        });
    }

    onPlayerReady() {
        
    }

    onPlayerStateChange(event:any) {
        if (event.data == YT.PlayerState.PLAYING) {
            setTimeout(this.stopVideo.bind(this), 1000 * this.sound.duration);
        }
    }
}