import { Component, Input } from '@angular/core';
import { Sound } from '../models/sound'
import { Settings } from '../util/settings';
import { AddSoundService } from '../services/addSound.service';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
  styleUrls: ['./sound.css'],
  selector: 'sound',
  templateUrl: './sound.html'
})

export class SoundComponent {
  readonly MAX_TIME = Settings.MAX_TIME;
  
  @Input() sound: Sound = new Sound()
  player:any
  submitPromise:any
  editing:boolean = false

  constructor(
    private addSoundService:AddSoundService, 
    private apiService:ApiService,
    private route: ActivatedRoute,
    private router:Router) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(this.onParams.bind(this));
  }

  onParams(params:Params) {
    if (params['id']) {
      this.apiService.getSound(params['id']).then(this.onSound.bind(this));
    }
  }

  onSound(sound:Sound) {
      this.sound = sound;
      this.editing = true;
  }

  test() {
    this.addSoundService.playVideo(this.sound);
  }

  submit() {
    this.submitPromise = this.apiService.addSound(this.sound).then(this.onSoundAdded.bind(this));
  }

  onSoundAdded(sound:Sound) {
    this.router.navigateByUrl('/sounds');
  }
}
